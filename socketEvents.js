var socketEvents = function(io) {

  io.on('connection', function(client) {
    console.log('Client connected...');
    
    client.on('sendMsg', function(data) {
      const newMessage = {
        is_admin: data.is_admin,
        name: data.name,
        header_id: data.header_id,
        messages: data.messages,
        token: data.token,
        status: data.status
      };
      
      client.emit('readMsg', newMessage);
      client.broadcast.emit('readMsg', newMessage);
    });
    
    client.on('sendRemoteRequest', (data) => {
      client.broadcast.emit('readRemoteRequest', data);
    });
  });
  
  io.on('disconnect', (client) => {
    console.log('Client disconnect...');
  });
}

module.exports = socketEvents;
