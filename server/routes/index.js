const messagesService = require('../services/messages')
, registersService = require('../services/registers')
, adminsService = require('../services/admins')
, remotesService = require('../services/remotes')
, devicesService = require('../services/devices')
, auth = require('../libs/auth');

function init(server) {
  server.get('*', function (req, res, next) {
    console.log('Request was made to: ' + req.originalUrl);
    return next();
  });
  
  server.get('/', function (req, res) {
    res.status(200).send({ message: "Hello World" });
  });
  
  server.get('/messages', auth.verifyToken, messagesService.getMessages);
  server.post('/messages', auth.verifyMachine, messagesService.createMessages);
  server.put('/messages_client/:headerId', auth.verifyMachine, messagesService.updateMessages);
  server.put('/messages_admin/:headerId', auth.verifyToken, messagesService.updateMessagesAdmin);
  server.get('/messages/:headerId', auth.verifyToken, messagesService.getMessageById);
  server.post('/update_status', auth.verifyToken, messagesService.updateDoneStatus);
  server.post('/update_status_pending', auth.verifyToken, messagesService.updatePendingStatus);
  
  server.post('/remote_helps', auth.verifyMachine, remotesService.postRemoteHelp);
  server.get('/remote_helps', auth.verifyToken, remotesService.getRemoteHelp);
  server.post('/update_status_remote', auth.verifyToken, remotesService.updateDoneStatus);
  server.post('/check_remote_id', auth.verifyToken, remotesService.getById);
  
  server.get('/devices', auth.verifyToken, devicesService.getDevices);
  server.post('/devices', auth.verifyToken, devicesService.createDevice);
  
  server.post('/upload_csv', auth.verifyToken, devicesService.uploadFileCsv);
  
  server.get('/check_token', auth.checkToken);
  
  server.post('/activate_registers', registersService.activateRegisters);
  
  server.post('/admin/registers', auth.verifyToken, adminsService.register);
  server.post('/admin/login', adminsService.login);
  server.get('/admins', auth.verifyToken, adminsService.getAdmins);
}

module.exports = {
  init: init
};