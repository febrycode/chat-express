module.exports = (sequelize, Sequelize) => {
	const HeaderMessage = sequelize.define('header_messages', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        deviceId: {
            type: Sequelize.INTEGER,
            field: 'device_id'
        },
        status: {
            type: Sequelize.STRING,
        },
        createdAt: {
            type: Sequelize.DATE,
            field: 'created_at'
        },
		updatedAt: {
			type: Sequelize.DATE,
			field: 'updated_at'
        },
        createdBy: {
            type: Sequelize.STRING,
            field: 'created_by'
        },
        updatedBy: {
            type: Sequelize.STRING,
            field: 'updated_by'
        }
	});
	
	return HeaderMessage;
}