module.exports = (sequelize, Sequelize) => {
	const RemoteRequest = sequelize.define('remote_requests', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        deviceId: {
            type: Sequelize.INTEGER,
            field: 'device_id'
        },
        password: {
            type: Sequelize.STRING,
            allowNull: false
        },
        status: {
            type: Sequelize.STRING
        },
        createdAt: {
            type: Sequelize.DATE,
            field: 'created_at'
        },
		updatedAt: {
			type: Sequelize.DATE,
			field: 'updated_at'
        },
        createdBy: {
            type: Sequelize.STRING,
            field: 'created_by'
        },
        updatedBy: {
            type: Sequelize.STRING,
            field: 'updated_by'
        }
	});
	
	return RemoteRequest;
}