module.exports = (sequelize, Sequelize) => {
	const Admin = sequelize.define('admins', {
		id: {
			type: Sequelize.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		username: {
			type: Sequelize.STRING,
			validate: {
				isUnique: (value, next) => {
					Admin.findOne({
						where: {
							username: value
						}
					})
					.then(
						admin => {
							if (admin) { return next('Username already in use') }
                            
							return next()
						}
					)
					.catch(
						err => {
							return next(err)
						}
					)
				}
			}
		},
		password: {
			type: Sequelize.STRING
		},
		fullname: {
			type: Sequelize.STRING
		},
		status: {
			type: Sequelize.STRING
		},
		lastLogin: {
			type: Sequelize.DATE,
			field: 'last_login'
		},
		createdAt: {
			type: Sequelize.DATE,
			field: 'created_at'
		},
		updatedAt: {
			type: Sequelize.DATE,
			field: 'updated_at'
		},
		createdBy: {
            type: Sequelize.STRING,
            field: 'created_by'
        },
        updatedBy: {
            type: Sequelize.STRING,
            field: 'updated_by'
		},
		level: {
			type: Sequelize.INTEGER
		}
	});
	
	return Admin;
}