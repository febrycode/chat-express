module.exports = (sequelize, Sequelize) => {
	const DetailMessage = sequelize.define('detail_messages', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        headerId: {
            type: Sequelize.INTEGER,
            field: 'header_id'
        },
        adminId: {
            type: Sequelize.INTEGER,
            field: 'admin_id'
        },
        isAdmin: {
            type: Sequelize.BOOLEAN,
            field: 'is_admin'
        },
        messages: {
            type: Sequelize.STRING
        },
        sent: {
            type: Sequelize.BOOLEAN
        },
        createdAt: {
            type: Sequelize.DATE,
            field: 'created_at'
        },
		updatedAt: {
			type: Sequelize.DATE,
			field: 'updated_at'
        },
        createdBy: {
            type: Sequelize.STRING,
            field: 'created_by'
        },
        updatedBy: {
            type: Sequelize.STRING,
            field: 'updated_by'
        }
	});
	
	return DetailMessage;
}