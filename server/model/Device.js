module.exports = (sequelize, Sequelize) => {
	const Device = sequelize.define('devices', {
		id: {
			type: Sequelize.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		serialNumber: {
			type: Sequelize.STRING,
      field: 'serial_number',
      validate: {
				isUnique: (value, next) => {
					Device.findOne({
						where: {
							serial_number: value
						}
					})
					.then(
						device => {
							if (device) { return next('Serial Number already in use') }
                            
							return next()
						}
					)
					.catch(
						err => {
							return next(err)
						}
					)
				}
			}
		},
		machineId: {
			type: Sequelize.STRING,
			field: 'machine_id'
		},
		remoteId: {
			type: Sequelize.STRING,
			field: 'remote_id'
		},
		registered: {
			type: Sequelize.BOOLEAN
		},
		token: {
			type: Sequelize.STRING
		},
		createdAt: {
			type: Sequelize.DATE,
			field: 'created_at'
		},
		updatedAt: {
			type: Sequelize.DATE,
			field: 'updated_at'
		},
		createdBy: {
      type: Sequelize.STRING,
      field: 'created_by'
    },
    updatedBy: {
      type: Sequelize.STRING,
      field: 'updated_by'
    },
    owner: {
      type: Sequelize.STRING,
    },
    location: {
      type: Sequelize.STRING
    }
	});
	
	return Device;
}