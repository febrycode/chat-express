const express = require('express')
, bodyParser = require('body-parser')
, cors = require('cors')
, fileUpload = require('express-fileupload')
, socketEvents = require("../socketEvents");

module.exports = function() {
    let server = express(),
        create,
        start;

    create = function(config) {
        let routes = require('./routes');

        // Server settings
        server.set('env', config.env);
        server.set('port', config.port);
        server.set('hostname', config.hostname);

        server.use(cors());

        // Returns middleware that parses json
        server.use(bodyParser.json());

        server.use(fileUpload());

        // Set up routes
        routes.init(server);
    };

    start = function() {
        let hostname = server.get('hostname'),
            port = server.get('port');

        const app = server.listen(port, function () {
            console.log('Express server listening on - http://' + hostname + ':' + port);
        });

        const io = require("socket.io").listen(app);

        socketEvents(io);
    };

    return {
        create: create,
        start: start
    };
};
