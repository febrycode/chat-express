const
    jwt = require('jsonwebtoken'),
    configs = require('../../configs/local'),
    db = require('../../configs/dbConfig'),
    Device = db.device;

function verifyToken(req, res, next) {
    const token = req.headers.authorization;
    if (!token) {
        return res.status(200).send({
            error: true,
            messages: "No token provide"
        })
    }

    jwt.verify(token, configs.secret, (err, decoded) => {
        if (err) {
            return res.status(200).send({
                error: true,
                messages: "Failed to authenticate token"
            });
        }

        // if everything good, save to request for use in other routes
        req.userId = decoded.id;
        req.username = decoded.username;
        next();
    });
}

function verifyMachine(req, res, next) {
    const token = req.headers.authorization;
    if (!token) {
        return res.status(200).send({
            error: true,
            messages: "No token provide"
        })
    }

    Device.findOne({
        where: {
            token: token
        }
    })
    .then(
        device => {
            if (!device) {
                return res.status(200).send({
                    error: true,
                    messages: "Device not found"
                })
            }

            req.deviceId = device.id;
            req.serialNumber = device.serialNumber;
            next();
        }
    )
    .catch(
        err => {
            return res.status(200).send({
                error: true,
                messages: err
            })
        }
    )
}

function checkToken(req, res) {
    const token = req.headers.authorization;
    
    if (!token) {
        return res.status(200).send({
            error: true,
            messages: "No token provide"
        })
    }

    jwt.verify(token, configs.secret, function(err, decoded) {
        if (err) {
            return res.status(200).send({
                error: true,
                messages: "Failed to authenticate token"
            })
        }
        
        return res.status(200).send({
            error: false,
            messages: "Success"
        })
    });
}

module.exports = {
    verifyToken,
    verifyMachine,
    checkToken
}