const bcrypt = require('bcryptjs')
, db = require('../../../configs/dbConfig')
, Device = db.device;

function activateRegisters(req, res) {
    const serialNumber = req.body.serial_number
    , remoteId = req.body.remote_id
    , machineId = req.body.machine_id;
    
    Device.findOne({
        where: {
            serialNumber: serialNumber
        }
    })
    .then(
        device => {
            if (!device) {
                return res.status(200).send({
                    error: true,
                    messages: "Invalid Data"
                });
            }
            
            hashedMachineId = bcrypt.hashSync(machineId, 8);
            
            device.machineId = machineId;
            device.remoteId = remoteId;
            device.registered = true;
            device.token = hashedMachineId;
            
            device.updatedBy = serialNumber;
            
            device.save()
            .then(
                () => {
                    res.status(200).send({
                        error: false,
                        token: hashedMachineId
                    });
                }
            )
            .catch(
                err => {
                    return res.status(200).send({
                        error: true,
                        messages: err
                    });
                }
            )
            
        }
    )
    .catch(
        err => {
            res.status(200).send({
                error: true,
                messages: err
            });
        }
    )
}

module.exports = {
    activateRegisters: activateRegisters
};
