const db = require('../../../configs/dbConfig')
, RemoteRequest = db.remoteRequst
, Device = db.device;

function postRemoteHelp(req, res) {
    const remotePassword = req.body.remote_password
    , token = req.headers.authorization;

    Device.findOne({
        where: {
            token: token
        }
    })
    .then(
        device => {
            if (!device) {
                return res.status(200).send({
                    error: true,
                    message: 'Device not found'
                })
            }
            RemoteRequest.findOne({
                where: {
                    deviceId: device.id
                }
            })
            .then(
                remoteRequest => {
                    if (remoteRequest) {
                        remoteRequest.password = remotePassword;
                        remoteRequest.status = 'New Request';
                        remoteRequest.updatedBy = device.serialNumber;
                        remoteRequest.save()
                        .then(
                            () => {
                                res.status(201).send({
                                    error: false,
                                    messages: "Data has been sent"
                                })
                            }
                        )
                        .catch(
                            err => {
                                res.status(200).send({
                                    error: true,
                                    messages: err
                                });
                            }
                        )
                    } else {
                        RemoteRequest.create({
                            deviceId: device.id,
                            password: remotePassword,
                            status: 'New Request',
                            createdBy: device.serialNumber,
                            updatedBy: device.serialNumber
                        })
                        .then(
                            () => {
                                return res.status(201).send({
                                    error: false,
                                    messages: "Data has been sent"
                                });
                            }
                        )
                        .catch(
                            err => {
                                return res.status(200).send({
                                    error: true,
                                    messages: err.errors.map((data) => {
                                        return {
                                            'key': data.path,
                                            'value': data.message
                                        }
                                    })
                                });
                            }
                        )
                    }
                }
            )
            .catch(
                err => {
                    return res.status(200).send({
                        error: true,
                        messages: err
                    })
                }
            )
        }
    )
    .catch(
        err => {
            res.status(200).send({
                error: true,
                messages: err
            });
        }
    )
}

function getRemoteHelp(req, res) {
    RemoteRequest.findAll({
        attributes: ['id', 'device_id', 'password', 'status', 'updated_at'],
        include: [
            {
                model: Device,
                attributes: ['serial_number', 'remote_id']
            }
        ]
    })
    .then(
        remoteRequests => {
            res.status(200).send({
                error: false,
                remoteRequests: remoteRequests
            })
        }
    )
    .catch(
        err => {
            res.status(200).send({
                error: true,
                messages: err
            });
        }
    )
}

function updateDoneStatus(req, res) {
    RemoteRequest.findById(req.body.id)
    .then(
        remoteRequest => {
            if (remoteRequest) {
                remoteRequest.status = 'Done'
                remoteRequest.save()
                .then(
                    () => {
                        res.status(204).send({
                            error: false,
                            messages: "success"
                        })
                    }
                )
                .catch(
                    err => {
                        res.status(200).send({
                            error: true,
                            messages: err
                        });
                    }
                )
            } else {
                res.status(200).send({
                    error: true,
                    messages: "Data not found"
                })
            }
        }
    )
    .catch(
        err => {
            res.status(200).send({
                error: true,
                messages: err
            });
        }
    )
}

function getById(req, res) {
    RemoteRequest.find({
        where: {
            id: req.body.id
        },
        attributes: ['id'],
        include: [
            {
                model: Device,
                attributes: ['remote_id']
            }
        ]
    })
    .then(
        remoteRequest => {
            res.status(200).send({
                error: false,
                remoteRequest
            });
        }
    )
    .catch(
        err => {
            res.status(200).send({
                error: true,
                messages: err
            }); 
        }
    )
}

module.exports = {
    postRemoteHelp,
    getRemoteHelp,
    updateDoneStatus,
    getById
};
