const db = require('../../../configs/dbConfig')
, HeaderMessage = db.headerMessage
, DetailMessage = db.detailMessage
, Device = db.device;

function getMessages(req, res) {
  HeaderMessage.findAll({
    order: [['updated_at', 'DESC']],
    attributes: ['id', 'status'],
    include: [
      {
        model: DetailMessage,
        attributes: ['id', 'header_id', 'messages'],
        limit: 1
      },
      {
        model: Device,
        attributes: ['serial_number', 'machine_id']
      }
    ]
  })
  .then(
    headerMessages => {
      res.status(200).send({
        error: false,
        messages: headerMessages
      })
    }
  )
  .catch(
    err => {
      res.status(200).send({
        error: true,
        messages: err
      });
    }
  )
}

function updateDoneStatus(req, res) {
  HeaderMessage.findById(req.body.id)
  .then(
    headerMessage => {
      if (headerMessage) {
        headerMessage.status = 'Closed'
        headerMessage.save()
        .then(
          () => {
            res.status(204).send({
              error: false,
              messages: "success"
            })
          }
        )
        .catch(
          err => {
            res.status(200).send({
              error: true,
              messages: err
            });
          }
        )
      } else {
        res.status(200).send({
          error: true,
          messages: "Data not found"
        })
      }
    }
  )
  .catch(
    err => {
      res.status(200).send({
        error: true,
        messages: err
      });
    }
  ) 
}

function updatePendingStatus(req, res) {
  HeaderMessage.findById(req.body.id)
  .then(
    headerMessage => {
      if (headerMessage) {
        headerMessage.status = 'Pending'
        headerMessage.save()
        .then(
          () => {
            res.status(204).send({
              error: false,
              messages: "success"
            })
          }
        )
        .catch(
          err => {
            res.status(200).send({
              error: true,
              messages: err
            });
          }
        )
      } else {
        res.status(200).send({
          error: true,
          messages: "Data not found"
        })
      }
    }
  )
  .catch(
    err => {
      res.status(200).send({
        error: true,
        messages: err
      });
    }
  )
}

// For Client
function createMessages(req, res) {
  const deviceId = req.deviceId
  , messages = req.body.messages
  , serialNumber = req.serialNumber;
  
  HeaderMessage.create({
    deviceId: deviceId,
    adminId: null,
    status: 'Open',
    createdBy: serialNumber,
    updatedBy: serialNumber
  })
  .then(
    headerMessage => {
      DetailMessage.create({
        headerId: headerMessage.id,
        adminId: null,
        isAdmin: 0,
        messages: messages,
        sent: 1,
        createdBy: serialNumber,
        updatedBy: serialNumber
      })
      .then(
        () => {
          res.status(201).send({
            error: false,
            header_id: headerMessage.id
          });  
        }
      )
      .catch(
        err => {
          res.status(200).send({
            error: true,
            messages: err
          });
        }
      )
    }
  )
  .catch(
    err => {
      res.status(200).send({
        error: true,
        messages: err
      });
    }
  )
}

// For Client
function updateMessages(req, res) {
  const deviceId = req.deviceId
  , headerId = req.params.headerId
  , serialNumber = req.serialNumber
  , messages = req.body.messages;
  
  HeaderMessage.findById(headerId)
  .then(
    headerMessage => {
      if (!headerMessage) {
        res.status(200).send({
          error: true,
          messages: "Message not found"
        });
      }
      
      headerMessage.status = 'Client Reply';
      headerMessage.updatedBy = serialNumber;
      headerMessage.save()
      .then(
        () => {
          DetailMessage.create({
            headerId: headerId,
            deviceId: deviceId,
            isAdmin: 0,
            messages: messages,
            sent: 1,
            createdBy: serialNumber,
            updatedBy: serialNumber
          })
          .then(
            res.status(201).send({
              error: false,
              messages: "success"
            })
          )
          .catch(
            err => {
              res.status(200).send({
                error: true,
                messages: err
              });
            }
          )
        }
      )
      .catch(
        err => {
          res.status(200).send({
            error: true,
            messages: err
          });
        }
      )
    }
  )
  .catch(
    err => {
      res.status(200).send({
        error: true,
        messages: err
      });
    }
  )
}

// For Admin
function updateMessagesAdmin(req, res) {
  const headerId = req.params.headerId
  , userId = req.userId
  , username = req.username
  , messages = req.body.messages;
  
  HeaderMessage.findById(headerId)
  .then(
    headerMessage => {
      headerMessage.status = 'Admin Reply';
      headerMessage.adminId = userId;
      
      headerMessage.save()
      .then(
        headerMessage => {
          
          DetailMessage.create({
            headerId: headerMessage.id,
            adminId: userId,
            isAdmin: 1,
            messages: messages,
            sent: 1,
            createdBy: username,
            updatedBy: username
          })
          .then(
            () => {
              res.status(201).send({
                error: false,
                messages: "success"
              });  
            }
          )
          .catch(
            err => {
              res.status(200).send({
                error: true,
                messages: err
              });
            }
          )
        }
      )
      .catch(
        err => {
          res.status(200).send({
            error: true,
            messages: err
          });
        }
      )
    }
  )
  .catch(
    err => {
      res.status(200).send({
        error: true,
        messages: err
      });
    }
  )
}

// For Admin
function getMessageById(req, res) {
  const headerId = req.params.headerId;
  
  DetailMessage.findAll({
    attributes: ['is_admin', 'messages'],
    where: {
      headerId: headerId
    },
    include: [
      {
        model: HeaderMessage,
        attributes: ['status']
      }
    ]
  })
  .then(
    detailMessages => {
      res.status(200).send({
        error: false,
        messages: "success",
        data: detailMessages
      })
    }
  )
  .catch(
    err => {
      res.status(200).send({
        error: true,
        messages: err
      });
    }
  )
}

module.exports = {
  getMessages,
  createMessages,
  updateDoneStatus,
  updatePendingStatus,
  updateMessages,
  getMessageById,
  updateMessagesAdmin
};
