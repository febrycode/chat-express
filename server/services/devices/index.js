const db = require('../../../configs/dbConfig')
, csv = require('fast-csv')
, multer = require('multer')
, Device = db.device;

function getDevices(req, res) {
  Device.findAll({
    order: [['id', 'ASC']],
    attributes: ['id', 'serial_number', 'machine_id', 'remote_id', 'owner', 'location'],
  })
  .then(
    devices => {
      res.status(200).send({
        error: false,
        messages: devices
      })
    }
  )
  .catch(
    err => {
      res.status(200).send({
        error: true,
        messages: err
      });
    }
  )
}

function createDevice(req, res) {
  const serialNumber = req.body.serial_number
  , remoteId = req.body.remote_id
  , owner = req.body.owner
  , location = req.body.location;
  
  Device.create({
    serialNumber: serialNumber,
    remoteId: remoteId,
    registered: 0,
    createdBy: 'Manual',
    updatedBy: 'Manual',
    owner: owner,
    location: location
  })
  .then(
    () => {
      return res.status(201).send({
        error: false,
        messages: "Device has been created"
      });
    }
  )
  .catch(
    err => {
      return res.status(200).send({
        error: true,
        messages: err.errors.map((data) => {
          return {
            'key': data.path,
            'value': data.message
          }
        })
      });
    }
  )
}

function uploadFileCsv(req, res) {
  const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, 'uploads/')
    },
    filename: function (req, file, cb) {
      cb(null, Date.now() + path.extname(file.originalname))
    }
  });
  
  const upload = multer({ storage: storage });

  upload.single('image')

  const deviceFile = req.files.file;
  const devices = [];
  
  csv
  .fromString(deviceFile.data.toString(), {
    headers : ["serialNumber", "owner", "location"],
    ignoreEmpty: true
  })
  .on("data", (data) => {
    devices.push(data)
  })
  .on("end", () => {
    devices.splice(0,1);
    
    Device.bulkCreate(devices)
      .then(
        () => {
          res.status(201).send({
            error: false,
            messages: "Data has been saved"
          });
        }
      )
      .catch(
        err => {
          res.status(200).send({
            error: true,
            messages: err.errors.map((data) => {
              return {
                'key': data.path,
                'value': data.message,
                'serial_number': data.value
              }
            })
          });
        }
      )
  })
}

module.exports = {
  getDevices,
  createDevice,
  uploadFileCsv
};
