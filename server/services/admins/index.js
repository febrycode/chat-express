const bcrypt = require('bcryptjs')
, jwt = require('jsonwebtoken')
, configs = require('../../../configs/local')
, db = require('../../../configs/dbConfig')
, Admin = db.admin;

function register(req, res) {
    const username = req.body.username
    , fullname = req.body.fullname
    , userCreated = req.username
    , hashedPassword = bcrypt.hashSync(req.body.password, 8);
    
    Admin.create({
        username: username,
        password: hashedPassword,
        fullname: fullname,
        status: 'Tidak Aktif',
        createdBy: userCreated,
        updatedBy: userCreated
    })
    .then(
        () => {
            return res.status(201).send({
                error: false,
                messages: "Data has been registered"
            });
        }
    )
    .catch(
        err => {
            return res.status(200).send({
                error: true,
                messages: err.errors.map((data) => {
                    return {
                        'key': data.path,
                        'value': data.message
                    }
                })
            });
        }
    )
}

function login(req, res) {
    const username = req.body.username
    , password = req.body.password;
    
    Admin.findOne({
        where: {
            username: username
        }
    })
    .then(
        admin => {
            if (!admin) {
                return res.status(200).send({
                    error: true,
                    messages: "Admin not found"
                });
            }
            
            const passwordIsValid = bcrypt.compareSync(password, admin.password);
            
            if (!passwordIsValid) {
                return res.status(200).send({
                    error: true,
                    messages: "Passwor invalid"
                });
            }
            
            const token = jwt.sign(
                {
                    id: admin.id,
                    username: admin.username
                },
                configs.secret,
                { expiresIn: 86400}
            );

            admin.lastLogin = Date.now();
            admin.save()
            .then(
                () => {
                    res.status(200).send({
                        error: false,
                        token: token,
                        fullname: admin.fullname
                    });
                }
            )
            .catch(
                err => {
                    return res.status(200).send({
                        error: true,
                        messages: err
                    });
                }
            )
        }
    )
    .catch(
        err => {
            res.status(200).send({
                error: true,
                messages: err
            });
        }
    );
}

function getAdmins(req, res) {
    Admin.findAll({
        order: [['id', 'ASC']],
        attributes: ['id', 'fullname', 'username', 'status', 'last_login']
    })
    .then(
        admins => {
            res.status(200).send({
                error: false,
                messages: admins
            })
        }
    )
    .catch(
        err => {
            res.status(200).send({
                error: true,
                messages: err
            });
        }
    )
}

module.exports = {
    register,
    login,
    getAdmins
};
