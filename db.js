var sql = require("mssql");

// config for your database
var config = {
    user: 'sa',
    password: 'p@55w0rd',
    server: "52.187.151.217\\sqlexpress", 
    database: 'support',

    options: {
        encrypt: true // Use this if you're on Windows Azure
    }
};

//Function to connect to database and execute query
const executeQuery = function (query) {
    return new Promise((resolve, reject) => {
        sql.connect(config, function (err) {
            if (err) {
                reject(err);
                sql.close();
            } else {
                // create Request object
                var request = new sql.Request();
                // query to the database
                request.query(query, function (err, resQuery) {
                    if (err) {
                        reject(err);
                        sql.close();
                    } else {
                        resolve(resQuery);
                        sql.close();
                    }
                });
            }
        });
    })
}

module.exports = executeQuery;