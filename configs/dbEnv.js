const dbEnv = {
    host: 'hostname',
    database: 'support',
    username: 'sa',
    password: 'p@55w0rd',
    dialect: 'mssql',
    host: "52.187.151.217",
    instance: "sqlexpress",
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    },
    dialectOptions: {
        encrypt: true
    }
  };
  
  module.exports = dbEnv;