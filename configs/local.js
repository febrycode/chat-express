let localConfig = {
    hostname: 'localhost',
    port: 3030,
    secret: 'supersecret'
};

module.exports = localConfig;
