const dbEnv = require('./dbEnv.js');

const Sequelize = require('sequelize');

// Override timezone formatting for MSSQL
Sequelize.DATE.prototype._stringify = function _stringify(date, options) {
  return this._applyTimezone(date, options).format('YYYY-MM-DD HH:mm:ss.SSS');
};

const sequelize = new Sequelize(
  dbEnv.database,
  dbEnv.username,
  dbEnv.password,
  {
    host: dbEnv.host,
    dialect: dbEnv.dialect,
    logging: false,

    operatorsAliases: false,

    port: 1433,
    pool: {
      max: 5,
      min: 0,
      idle: 10000
    },
    
    dialectOptions: {
      encrypt: true,
      instanceName: dbEnv.instance
    }
  }
);

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.admin = require('../server/model/Admin.js')(sequelize, Sequelize);
db.headerMessage = require('../server/model/HeaderMessage')(sequelize, Sequelize);
db.detailMessage = require('../server/model/DetailMessage')(sequelize, Sequelize);
db.device = require('../server/model/Device')(sequelize, Sequelize);
db.remoteRequst = require('../server/model/RemoteRequest')(sequelize, Sequelize);

// Relations
db.headerMessage.hasMany(
  db.detailMessage,
  {
    foreignKey: 'header_id',
    sourceKey: 'id'
  }
);

db.detailMessage.belongsTo(
  db.headerMessage,
  {
    foreignKey: 'header_id',
    targetKey: 'id'
  }
);

db.device.hasMany(
  db.headerMessage,
  {
    foreignKey: 'device_id',
    sourceKey: 'id'
  }
);

db.headerMessage.belongsTo(
  db.device,
  {
    foreignKey: 'device_id',
    targetKey: 'id'
  }
);

db.device.hasMany(
  db.remoteRequst,
  {
    foreignKey: 'device_id',
    sourceKey: 'id'
  }
);

db.remoteRequst.belongsTo(
  db.device,
  {
    foreignKey: 'device_id',
    targetKey: 'id'
  }
);

module.exports = db;